import { uuid } from '../../utils/uuid'
import { ADD_LIST, ADD_TASK, REORDER, MOVE } from './actions'

export const initialState = {
  lists: [
    {
      id: uuid(),
      title: 'Backlogs',
      tasks: [
        { id: uuid(), title: 'Task 1' },
        { id: uuid(), title: 'Task 2' },
      ],
    },
    {
      id: uuid(),
      title: 'In Progress',
      tasks: [
        { id: uuid(), title: 'Task 3' },
        { id: uuid(), title: 'Task 4' },
      ],
    },
    {
      id: uuid(),
      title: 'Review',
      tasks: [
        { id: uuid(), title: 'Task 5' },
        { id: uuid(), title: 'Task 6' },
      ],
    },
  ],
}

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}

const move = (source, destination, sIndex, dIndex) => {
  const sourceClone = Array.from(source)
  const destClone = Array.from(destination)

  const [removed] = sourceClone.splice(sIndex, 1)
  destClone.splice(dIndex, 0, removed)

  const result = {
    source: sourceClone,
    destination: destClone,
  }
  return result
}

const getListById = (state, id) => {
  let list = state.lists.find((list) => list.id === id)
  return list
}

export function reducer(state, action) {
  switch (action.type) {
    case ADD_LIST: {
      const newList = {
        id: uuid(),
        title: action.payload.title,
        tasks: [],
      }
      return { lists: [...state.lists, newList] }
    }

    case ADD_TASK: {
      const task = {
        id: uuid(),
        title: action.payload.title,
      }

      let updatedList = getListById(state, action.payload.listId)

      updatedList.tasks = [task, ...updatedList.tasks]

      return { lists: state.lists.map((list) => (list.id === action.payload.listId ? updatedList : list)) }
    }

    case REORDER: {
      let updatedList = getListById(state, action.payload.listId)
      const updatedTasks = reorder(updatedList.tasks, action.payload.sIndex, action.payload.dIndex)
      updatedList.tasks = updatedTasks

      return { lists: state.lists.map((list) => (list.id === action.payload.listId ? updatedList : list)) }
    }

    case MOVE: {
      const { fromListId, toListId, sIndex, dIndex } = action.payload

      const fromList = getListById(state, fromListId)
      const toList = getListById(state, toListId)

      const result = move(fromList.tasks, toList.tasks, sIndex, dIndex)

      const newLists = state.lists.map((list) => {
        if (list.id === fromListId) return { ...list, tasks: result.source }
        if (list.id === toListId) return { ...list, tasks: result.destination }
        return list
      })

      return { lists: newLists }
    }

    default: {
      return state
    }
  }
}

export const cachedReducer = (state, action) => {
  const savedState = reducer(state, action)
  const json = JSON.stringify(savedState)
  localStorage.setItem('store', json)

  return savedState
}

export const cachedState = () => {
  try {
    const json = localStorage.getItem('store')

    const savedState = JSON.parse(json)
    if (!savedState) throw new Error()

    return savedState
  } catch (e) {
    return initialState
  }
}
