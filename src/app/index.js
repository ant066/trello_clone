import React, { useReducer, useState } from 'react'
import Layout from '../components/layout'
import List from '../components/list'
import { cachedState, cachedReducer } from './store/reducer'
import './styles/reset.css'
import './styles/app.scss'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { MOVE, REORDER } from './store/actions'

const AppContext = React.createContext()

function App() {
  const [state, dispatch] = useReducer(cachedReducer, cachedState())
  const [isAddList, setAddList] = useState(false)

  const { lists } = state

  function onDragEnd(result) {
    const { source, destination } = result
    if (!destination) return

    const sInd = source.droppableId
    const dInd = destination.droppableId

    if (dInd === sInd) {
      dispatch({
        type: REORDER,
        payload: {
          listId: sInd,
          sIndex: source.index,
          dIndex: destination.index,
        },
      })
      return
    }

    dispatch({
      type: MOVE,
      payload: {
        fromListId: sInd,
        toListId: dInd,
        sIndex: source.index,
        dIndex: destination.index,
      },
    })
  }

  const onCloseAddList = () => {
    setAddList(false)
  }
  const onAddNewList = () => {
    setAddList(true)
  }

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="board" type="COLUMN" direction="horizontal" ignoreContainerClipping isCombineEnabled>
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps} style={{ height: '100vh' }}>
              <Layout>
                {lists.map((list, index) => (
                  <List key={index} index={index} title={list.title} tasks={list.tasks} id={list.id} />
                ))}
                {isAddList ? (
                  <List isNew onClose={onCloseAddList} />
                ) : (
                  <button className="add-list-btn" onClick={onAddNewList}>
                    ADD LIST
                  </button>
                )}
              </Layout>
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </AppContext.Provider>
  )
}

export { App as default, AppContext }
