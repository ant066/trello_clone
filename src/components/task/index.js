import './styles.scss'
import React, { useState, useContext } from 'react'
import { AppContext } from '../../app'
import { ADD_TASK } from '../../app/store/actions'

export const Task = ({ title, isNew = false, listId, onClose, provided }) => {
  const [value, setValue] = useState('')
  const { dispatch } = useContext(AppContext)

  const onChange = (evt) => {
    const { value } = evt.target
    setValue(value)
  }

  const onSave = () => {
    dispatch({
      type: ADD_TASK,
      payload: {
        title: value,
        listId: listId,
      },
    })
    onClose()
  }

  if (isNew) {
    return (
      <div className="new-task">
        <input value={value} onChange={onChange}></input>
        <div className="control">
          <button onClick={onSave} className="save">
            Save
          </button>
          <button onClick={onClose} className="cancel">
            Cancel
          </button>
        </div>
      </div>
    )
  }
  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      style={provided.draggableProps.style}
      className="task"
    >
      <div>{title}</div>
    </div>
  )
}

export default Task
